import "package:baam/bloc/checkin/exceptions.dart";
import "package:baam/network/models/baam_models.dart";
import "package:baam/network/repository/baam_repository.dart";
import "package:equatable/equatable.dart";
import "package:flutter_bloc/flutter_bloc.dart";

part "checkin_event.dart";
part "checkin_state.dart";

class CheckInBloc extends Bloc<CheckInEvent, CheckInState> {
  CheckInBloc(this.repository) : super(ScanningState()) {
    on<ParseAndSubmitCodeEvent>(_parseAndSubmitCodeEvent);
  }

  final BaamRepository repository;

  Future<void> _parseAndSubmitCodeEvent(ParseAndSubmitCodeEvent e, Emitter emit) async {
    if (state is! ScanningState) return;

    const String baamStartUrl = "https://baam.duckdns.org/s#";
    if (!e.url.startsWith(baamStartUrl)) {
      emit(ScanningState(exception: MalformedQRCode()));
      return;
    }

    final List<String> sessionIdCode = e.url.substring(baamStartUrl.length).split("-");
    if (sessionIdCode.length != 2) {
      emit(ScanningState(exception: MalformedQRCode()));
      return;
    }
    final String sessionId = sessionIdCode[0];
    final String code = sessionIdCode[1];

    final MarkedSessionInfo info;
    try {
      await repository.submitChallenge(sessionId, code);
      info = await repository.getMarkedSessionInfo(sessionId);
    } on Exception catch (e) {
      emit(ScanningState(exception: e));
      return;
    }

    if (state is CheckedInState) return;
    emit(CheckedInState(info));
  }
}
