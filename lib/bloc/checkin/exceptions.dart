import "package:equatable/equatable.dart";

class CheckInException extends Equatable implements Exception {
  @override
  List<Object> get props => [];
}

class MalformedQRCode extends CheckInException {}
