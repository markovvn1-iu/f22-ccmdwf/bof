part of "checkin_bloc.dart";

abstract class CheckInEvent {}

class ParseAndSubmitCodeEvent extends CheckInEvent {
  ParseAndSubmitCodeEvent(this.url);
  String url;
}
