part of "checkin_bloc.dart";

abstract class CheckInState extends Equatable {}

class ScanningState extends CheckInState {
  ScanningState({this.exception});
  final Exception? exception;

  @override
  List<Object?> get props => [exception];
}

class CheckedInState extends CheckInState {
  CheckedInState(this.info);
  final MarkedSessionInfo info;

  @override
  List<Object> get props => [info];
}
