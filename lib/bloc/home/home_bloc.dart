import "package:baam/network/exceptions/baam_exceptions.dart";
import "package:baam/network/models/baam_models.dart";
import "package:baam/network/repository/baam_repository.dart";
import "package:equatable/equatable.dart";
import "package:flutter_bloc/flutter_bloc.dart";

part "home_event.dart";
part "home_state.dart";

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  HomeBloc(this.repository) : super(InitialState()) {
    on<CheckAuthEvent>(_checkAuthEvent);
    on<LoginEvent>(_loginEvent);
    on<LogoutEvent>(_logoutEvent);
  }
  final BaamRepository repository;

  Future<void> _checkAuthEvent(CheckAuthEvent e, Emitter emit) async {
    try {
      final userInfo = await repository.getUserInfo();
      emit(AuthorizedState(userInfo));
    } on NotAuthorizedException catch (err) {
      // TODO: Unhandled Exception: Instance of "ConnectionFailedException"
      emit(NotAuthorizedState(err.authUrl));
    }
  }

  Future<void> _loginEvent(LoginEvent e, Emitter emit) async {
    repository.setToken(e.token);
    await _checkAuthEvent(CheckAuthEvent(), emit);
  }

  Future<void> _logoutEvent(LogoutEvent e, Emitter emit) async {
    await repository.logout();
    await _checkAuthEvent(CheckAuthEvent(), emit);
  }
}
