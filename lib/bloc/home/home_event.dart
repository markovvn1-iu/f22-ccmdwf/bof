part of "home_bloc.dart";

abstract class HomeEvent {}

class CheckAuthEvent extends HomeEvent {}

class LoginEvent extends HomeEvent {
  LoginEvent(this.token);
  String token;
}

class LogoutEvent extends HomeEvent {}
