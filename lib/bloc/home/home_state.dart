part of "home_bloc.dart";

abstract class HomeState extends Equatable {
  @override
  List<Object> get props => [];
}

class InitialState extends HomeState {}

class AuthorizedState extends HomeState {
  AuthorizedState(this.userInfo);
  final UserInfo userInfo;

  @override
  List<Object> get props => [userInfo];
}

class NotAuthorizedState extends HomeState {
  NotAuthorizedState(this.authUrl);
  final String authUrl;

  @override
  List<Object> get props => [authUrl];
}
