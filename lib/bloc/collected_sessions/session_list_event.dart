part of "session_list_bloc.dart";

abstract class SessionListEvent {}

class SessionListLoadEvent extends SessionListEvent {}

class SessionListReloadEvent extends SessionListEvent {}

class SessionListDeleteEvent extends SessionListEvent {
  SessionListDeleteEvent(this.sessionInfo);

  final PartialSessionInfo sessionInfo;
}

class WidgetNotifiedEvent extends SessionListEvent {}
