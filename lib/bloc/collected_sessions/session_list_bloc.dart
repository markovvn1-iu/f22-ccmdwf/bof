import "dart:async";

import "package:baam/network/exceptions/baam_exceptions.dart";
import "package:baam/network/models/baam_models.dart";
import "package:baam/network/repository/baam_repository.dart";
import "package:equatable/equatable.dart";
import "package:flutter_bloc/flutter_bloc.dart";

part "session_list_event.dart";
part "session_list_state.dart";

class SessionListBloc extends Bloc<SessionListEvent, SessionListState> {
  SessionListBloc(this.repository) : super(SessionListInitial()) {
    on<SessionListLoadEvent>(_loadSessions);
    on<SessionListReloadEvent>(_reloadSessions);
    on<SessionListDeleteEvent>(_deleteSession);
    on<WidgetNotifiedEvent>(_onWidgetNotified);
  }

  final BaamRepository repository;
  final List<PartialSessionInfo> sessions = [];

  Future<void> _loadSessions(SessionListLoadEvent event, Emitter emit) async {
    try {
      sessions.clear();
      sessions.addAll(await repository.getCollectedSessions());
      emit(SessionListLoadedState());
    } on ConnectionFailedException {
      emit(ConnectionFailedState(needReload: true));
    }
  }

  Future<void> _reloadSessions(SessionListReloadEvent event, Emitter emit) async {
    emit(SessionListInitial());
  }

  Future<void> _deleteSession(SessionListDeleteEvent event, Emitter emit) async {
    try {
      await repository.deleteSession(event.sessionInfo.id);
      sessions.remove(event.sessionInfo);
      emit(SessionDeletedState());
    } on ConnectionFailedException {
      emit(ConnectionFailedState(needReload: false));
    }
  }

  Future<void> _onWidgetNotified(WidgetNotifiedEvent event, Emitter emit) async {
    emit(SessionListLoadedState());
  }
}
