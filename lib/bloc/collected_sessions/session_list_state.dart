part of "session_list_bloc.dart";

abstract class SessionListState extends Equatable {
  @override
  List<Object> get props => [];
}

class SessionListInitial extends SessionListState {}

class SessionListLoadedState extends SessionListState {}

class ConnectionFailedState extends SessionListState {
  ConnectionFailedState({this.needReload = false});

  final bool needReload;

  @override
  List<Object> get props => [needReload];
}

class SessionDeletedState extends SessionListState {}
