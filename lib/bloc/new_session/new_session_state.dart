part of "new_session_bloc.dart";

abstract class NewSessionState extends Equatable {
  const NewSessionState({bool? isConnectionStable, this.exception}) : isConnectionStable = isConnectionStable ?? true;

  final bool isConnectionStable;
  final Exception? exception;

  NewSessionState clone({bool? isConnectionStable, Exception? exception});

  @override
  List<Object> get props => [isConnectionStable, if (exception != null) exception!];
}

class InitialState extends NewSessionState {
  const InitialState({bool? isConnectionStable, Exception? exception})
      : super(isConnectionStable: isConnectionStable, exception: exception);
  @override
  InitialState clone({bool? isConnectionStable, Exception? exception}) {
    return InitialState(isConnectionStable: isConnectionStable, exception: exception);
  }
}

class LoadingState extends NewSessionState {
  const LoadingState({bool? isConnectionStable, Exception? exception})
      : super(isConnectionStable: isConnectionStable, exception: exception);
  @override
  LoadingState clone({bool? isConnectionStable, Exception? exception}) {
    return LoadingState(isConnectionStable: isConnectionStable, exception: exception);
  }
}

class RunningSessionState extends NewSessionState {
  const RunningSessionState(this.info, this.checkInUrl, {bool? isConnectionStable, Exception? exception})
      : super(isConnectionStable: isConnectionStable, exception: exception);
  final SessionInfo info;
  final String checkInUrl;

  @override
  RunningSessionState clone({bool? isConnectionStable, Exception? exception}) {
    return RunningSessionState(info, checkInUrl, isConnectionStable: isConnectionStable, exception: exception);
  }

  @override
  List<Object> get props => [isConnectionStable, if (exception != null) exception!, info, checkInUrl];
}

class StopSessionLoadingState extends NewSessionState {
  const StopSessionLoadingState(this.info, {bool? isConnectionStable, Exception? exception})
      : super(isConnectionStable: isConnectionStable, exception: exception);
  final SessionInfo info;

  @override
  StopSessionLoadingState clone({bool? isConnectionStable, Exception? exception}) {
    return StopSessionLoadingState(info, isConnectionStable: isConnectionStable, exception: exception);
  }

  @override
  List<Object> get props => [isConnectionStable, if (exception != null) exception!, info];
}

class StoppedSessionState extends NewSessionState {
  const StoppedSessionState(this.info, {bool? isConnectionStable, Exception? exception})
      : super(isConnectionStable: isConnectionStable, exception: exception);
  final SessionInfo info;

  @override
  StoppedSessionState clone({bool? isConnectionStable, Exception? exception}) {
    return StoppedSessionState(info, isConnectionStable: isConnectionStable, exception: exception);
  }

  @override
  List<Object> get props => [isConnectionStable, if (exception != null) exception!, info];
}
