import "dart:async";

import "package:baam/network/exceptions/baam_exceptions.dart";
import "package:baam/network/models/baam_models.dart";
import "package:baam/network/repository/baam_repository.dart";
import "package:baam/utils/precise_timer.dart";

import "package:equatable/equatable.dart";
import "package:flutter_bloc/flutter_bloc.dart";

part "new_session_event.dart";
part "new_session_state.dart";

class NewSessionBloc extends Bloc<NewSessionEvent, NewSessionState> {
  NewSessionBloc(this.repository) : super(const InitialState()) {
    on<_UpdateCodeEvent>(_updateCodeEvent);
    on<_UpdateSessionInfoEvent>(_updateSessionInfoEvent);
    on<GetOrStartSessionEvent>(_getOrStartSessionEvent);
    on<StopSessionEvent>(_stopSessionEvent);
    on<ResetSessionEvent>(_resetSessionEvent);
    on<RenameSessionEvent>(_renameSessionEvent);
    on<AddUserEvent>(_addUserEvent);
  }

  final BaamRepository repository;
  RunningSessionInfo? runningSessionInfo;
  SessionInfo? activeSessionInfo;
  PreciseTimer? codeTimer;
  Timer? updateInfoTimer;
  bool gettingSession = false;
  bool gettingSessionUpdate = false;
  bool isWorking = true;

  // For updaters
  bool connectionState = true;
  String? urlCode;

  @override
  Future<void> close() {
    isWorking = false;
    codeTimer?.stop();
    updateInfoTimer?.cancel();
    return super.close();
  }

  Future<void> _updateCodeEvent(_UpdateCodeEvent event, Emitter emit) async {
    if (runningSessionInfo == null || activeSessionInfo == null || codeTimer == null) return;
    RunningSessionInfo sessionInfo = runningSessionInfo!;

    final String code = sessionInfo.getAttendanceCode(codeTimer!.count() + sessionInfo.initialIteration);
    urlCode = "https://baam.duckdns.org/s#${sessionInfo.id}-$code";
    emit(RunningSessionState(activeSessionInfo!, urlCode!).clone(isConnectionStable: connectionState));
  }

  Future<void> _updateSessionInfoEvent(_UpdateSessionInfoEvent event, Emitter emit) async {
    if (activeSessionInfo == null || gettingSessionUpdate) return;
    SessionInfo sessionInfo = activeSessionInfo!;
    gettingSessionUpdate = true;

    SessionInfo? updatedSession;
    Exception? exception;
    try {
      updatedSession = await repository.getSessionInfo(sessionInfo.id);
    } on SessionNotFoundException {
      return await _getOrStartSessionEvent(GetOrStartSessionEvent(), emit);
    } on Exception catch (err) {
      exception = err;
    }
    gettingSessionUpdate = false;

    if (activeSessionInfo != sessionInfo) return; // If the session was not changed
    if (updatedSession == activeSessionInfo && (!connectionState) && (exception is ConnectionFailedException)) {
      return; // If no updates
    }

    if (updatedSession != null) {
      activeSessionInfo = updatedSession;
    }
    connectionState = exception is! ConnectionFailedException;

    NewSessionState newState;
    if (state is RunningSessionState && urlCode != null) {
      newState = RunningSessionState(activeSessionInfo!, urlCode!);
    } else if (state is StoppedSessionState) {
      newState = StoppedSessionState(activeSessionInfo!);
    } else {
      newState = state;
    }

    if (exception != null) {
      if (exception is ConnectionFailedException) {
        newState = newState.clone(isConnectionStable: connectionState);
      } else {
        newState = newState.clone(exception: exception);
      }
    }
    emit(newState);
  }

  void _stopSession() {
    runningSessionInfo = null;
    codeTimer?.stop();
    updateInfoTimer?.cancel();
    codeTimer = null;
    updateInfoTimer = null;
    urlCode = null;
    gettingSessionUpdate = false;
  }

  Future<void> _getOrStartSessionEvent(GetOrStartSessionEvent event, Emitter emit) async {
    if (gettingSession) return;
    gettingSession = true;

    _stopSession();
    activeSessionInfo = null;
    emit(const LoadingState());

    RunningSessionInfo? newSession;
    while (isWorking && newSession == null) {
      try {
        newSession = await repository.getOrStartSession();
      } on ConnectionFailedException {
        emit(const LoadingState().clone(isConnectionStable: false));
      } on Exception catch (err) {
        emit(const LoadingState().clone(exception: err));
      }
    }
    gettingSession = false;
    if (newSession == null) return;
    emit(const LoadingState());

    runningSessionInfo = newSession;
    activeSessionInfo = SessionInfo(newSession.id, newSession.title, newSession.users);
    codeTimer = PreciseTimer(
      Duration(milliseconds: newSession.secretCodeIntervalMs),
      () => add(_UpdateCodeEvent()),
    );
    updateInfoTimer = Timer.periodic(
      const Duration(seconds: 1),
      (_) => add(_UpdateSessionInfoEvent()),
    );
  }

  Future<void> _stopSessionEvent(StopSessionEvent event, Emitter emit) async {
    if (activeSessionInfo == null) return;
    SessionInfo sessionInfo = activeSessionInfo!;
    _stopSession();

    emit(StopSessionLoadingState(sessionInfo).clone(isConnectionStable: connectionState));

    while (isWorking) {
      try {
        await repository.stopSession(sessionInfo.id);
        break;
      } on ConnectionFailedException {
        connectionState = false;
        emit(StopSessionLoadingState(sessionInfo).clone(isConnectionStable: connectionState));
      } on SessionNotFoundException catch (err) {
        emit(state.clone(exception: err));
        break; // hope this session was stopped before
      } on Exception catch (err) {
        emit(state.clone(exception: err));
      }
    }
    if (!isWorking) return;

    connectionState = true;
    emit(StoppedSessionState(sessionInfo));
  }

  Future<void> _resetSessionEvent(ResetSessionEvent event, Emitter emit) async {
    if (activeSessionInfo == null) return;
    SessionInfo sessionInfo = activeSessionInfo!;
    _stopSession();
    activeSessionInfo = null;

    try {
      await repository.deleteSession(sessionInfo.id);
    } on Exception catch (err) {
      emit(state.clone(exception: err));
    }

    return await _getOrStartSessionEvent(GetOrStartSessionEvent(), emit);
  }

  Future<void> _renameSessionEvent(RenameSessionEvent event, Emitter emit) async {
    if (activeSessionInfo == null) return;
    SessionInfo sessionInfo = activeSessionInfo!;

    try {
      await repository.changeSessionInfo(sessionInfo.id, title: event.newTitle);
    } on SessionNotFoundException catch (err) {
      emit(state.clone(exception: err));
      return await _getOrStartSessionEvent(GetOrStartSessionEvent(), emit);
    } on Exception catch (err) {
      emit(state.clone(exception: err));
    }

    return await _updateSessionInfoEvent(_UpdateSessionInfoEvent(), emit);
  }

  Future<void> _addUserEvent(AddUserEvent event, Emitter emit) async {
    if (activeSessionInfo == null) return;
    SessionInfo sessionInfo = activeSessionInfo!;

    try {
      await repository.addUserToSession(sessionInfo.id, userEmail: event.email);
    } on SessionNotFoundException catch (err) {
      emit(state.clone(exception: err));
      return await _getOrStartSessionEvent(GetOrStartSessionEvent(), emit);
    } on Exception catch (err) {
      emit(state.clone(exception: err));
    }

    return await _updateSessionInfoEvent(_UpdateSessionInfoEvent(), emit);
  }
}
