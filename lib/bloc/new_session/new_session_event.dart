part of "new_session_bloc.dart";

abstract class NewSessionEvent {}

class _UpdateCodeEvent extends NewSessionEvent {} // Technical event

class _UpdateSessionInfoEvent extends NewSessionEvent {} // Technical event

class GetOrStartSessionEvent extends NewSessionEvent {}

class StopSessionEvent extends NewSessionEvent {}

class ResetSessionEvent extends NewSessionEvent {}

class RenameSessionEvent extends NewSessionEvent {
  RenameSessionEvent(this.newTitle);
  final String newTitle;
}

class AddUserEvent extends NewSessionEvent {
  AddUserEvent(this.email);
  final String email;
}
