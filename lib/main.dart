import "dart:ui";

import "package:baam/locator.dart";
import "package:baam/routes.dart";
import "package:baam/screens/collected_sessions/collected_sessions.dart";
import "package:baam/screens/checkin/checkin.dart";
import "package:baam/screens/home.dart";
import "package:baam/screens/new_session/new_session.dart";
import "package:firebase_crashlytics/firebase_crashlytics.dart";
import "package:flutter/material.dart";
import "package:flutter_localizations/flutter_localizations.dart";
import "package:flutter_gen/gen_l10n/app_localizations.dart";
import "package:page_transition/page_transition.dart";
import "package:firebase_core/firebase_core.dart";
import "package:baam/firebase_options.dart";

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await Firebase.initializeApp(options: DefaultFirebaseOptions.currentPlatform);
  // Pass all uncaught errors from the framework to Crashlytics.
  FlutterError.onError = FirebaseCrashlytics.instance.recordFlutterFatalError;
  // Pass all uncaught asynchronous errors that aren't handled by the Flutter framework to Crashlytics
  PlatformDispatcher.instance.onError = (error, stack) {
    FirebaseCrashlytics.instance.recordError(error, stack, fatal: true);
    return true;
  };

  initLocator();
  runApp(const BaamApp());
}

class BaamApp extends StatelessWidget {
  const BaamApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      localizationsDelegates: const [
        AppLocalizations.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: const [
        Locale("en", ""),
        Locale("ar", ""),
        Locale("ru", ""),
      ],
      theme: ThemeData(
        brightness: Brightness.light,
      ),
      darkTheme: ThemeData(brightness: Brightness.dark),
      themeMode: ThemeMode.system,
      initialRoute: Routes.home,
      onGenerateRoute: (settings) {
        switch (settings.name) {
          case Routes.home:
            return PageTransition(
                child: const HomeScreen(), type: PageTransitionType.fade, duration: const Duration(milliseconds: 300));
          case Routes.newSession:
            return PageTransition(
                child: NewSessionScreen(),
                childCurrent: this,
                type: PageTransitionType.leftToRightPop,
                duration: const Duration(milliseconds: 300));
          case Routes.checkIn:
            return PageTransition(
                child: const CheckInScreen(),
                childCurrent: this,
                type: PageTransitionType.rightToLeftJoined,
                duration: const Duration(milliseconds: 300));
          case Routes.collectedSessions:
            return PageTransition(
                child: const CollectedSessionsScreen(),
                childCurrent: this,
                type: PageTransitionType.bottomToTopJoined,
                duration: const Duration(milliseconds: 300));
          default:
            return null;
        }
      },
    );
  }
}
