import "package:baam/network/api/baam_api.dart";
import "package:baam/network/exceptions/baam_exceptions.dart";
import "package:baam/network/models/baam_models.dart";
import "package:dio/dio.dart";
import "package:flutter_secure_storage/flutter_secure_storage.dart";

part "baam_repository_impl.dart";

abstract class BaamRepository {
  factory BaamRepository(BaamApi api) {
    return _BaamRepository(api);
  }

  void setToken(String token);

  Future<String?> getToken();

  Future<void> logout();

  /// Get information about the user.
  Future<UserInfo> getUserInfo();

  /// Submit a scanned code to check in.
  Future<void> submitChallenge(String sessionId, String code);

  /// Get information about the MarkedSuccessfully page (after check in)
  Future<MarkedSessionInfo> getMarkedSessionInfo(String sessionId);

  /// Get current session or create the new one.
  Future<RunningSessionInfo> getOrStartSession();

  /// Stop the active session
  Future<void> stopSession(String sessionId);

  /// Continue the stopped session
  Future<RunningSessionInfo> continueSession(String sessionId);

  /// Delete the session. Works with both active and past sessions
  Future<void> deleteSession(String sessionId);

  /// Get basic information about the session. Works with both active and past sessions
  Future<SessionInfo> getSessionInfo(String sessionId);

  /// Change some information about the session. Works with both active and past sessions
  Future<void> changeSessionInfo(String sessionId, {required String title});

  /// Change some information about the session. Works with both active and past sessions
  Future<void> addUserToSession(String sessionId, {required String userEmail});

  /// Get past sessions
  Future<List<PartialSessionInfo>> getCollectedSessions();
}
