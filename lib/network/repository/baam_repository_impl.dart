part of "baam_repository.dart";

class _BaamRepository implements BaamRepository {
  _BaamRepository(this._api);

  final FlutterSecureStorage _storage = const FlutterSecureStorage();
  String? _token;
  bool readTokenFromStorage = true;

  final BaamApi _api;

  @override
  void setToken(String token) {
    _token = token;
    _storage.write(key: "token", value: token);
  }

  @override
  Future<String?> getToken() async {
    if (readTokenFromStorage) {
      readTokenFromStorage = false;
      _token = await _storage.read(key: "token");
    }
    return _token;
  }

  void _checkAuthRedirect(Response response) {
    if (response.statusCode != 302) return;
    final location = response.headers.value("Location");
    if (location == null) return;
    if (Uri.parse(location).host != "sso.university.innopolis.ru") return;
    throw NotAuthorizedException(location);
  }

  Future<Exception> _throwNotAuthorized() async {
    try {
      await _api.getRootPage();
    } catch (err) {
      if (err is DioError) {
        if (err.response == null) throw ConnectionFailedException();
        _checkAuthRedirect(err.response!);
      }
    }
    return UnknownBaamApiException();
  }

  @override
  Future<void> logout() async {
    _token = null;
    await _storage.write(key: "token", value: null);
  }

  @override
  Future<UserInfo> getUserInfo() async {
    final String? token = await getToken();
    if (token == null) throw await _throwNotAuthorized();
    try {
      return await _api.getUserInfo(token);
    } catch (err) {
      if (err is DioError) {
        if (err.response == null) throw ConnectionFailedException();
        _checkAuthRedirect(err.response!);
      }
    }
    throw UnknownBaamApiException();
  }

  @override
  Future<void> submitChallenge(String sessionId, String code) async {
    final String? token = await getToken();
    if (token == null) throw await _throwNotAuthorized();
    try {
      return await _api.submitChallenge(token, sessionId, code);
    } catch (err) {
      if (err is DioError) {
        if (err.response == null) throw ConnectionFailedException();
        _checkAuthRedirect(err.response!);
        if (err.response!.statusCode == 302) throw CodeIsUnacceptableException();
        if (err.response!.statusCode == 404) throw SessionNotFoundException();
      }
    }
    throw UnknownBaamApiException();
  }

  @override
  Future<MarkedSessionInfo> getMarkedSessionInfo(String sessionId) async {
    final String? token = await getToken();
    if (token == null) throw await _throwNotAuthorized();
    try {
      return await _api.getMarkedSessionInfo(token, sessionId);
    } catch (err) {
      if (err is DioError) {
        if (err.response == null) throw ConnectionFailedException();
        _checkAuthRedirect(err.response!);
        if (err.response!.statusCode == 404) throw UserNotCheckedInForThisSessionException();
      }
    }
    throw UnknownBaamApiException();
  }

  @override
  Future<RunningSessionInfo> getOrStartSession() async {
    final String? token = await getToken();
    if (token == null) throw await _throwNotAuthorized();
    try {
      return await _api.getOrStartSession(token);
    } catch (err) {
      if (err is DioError) {
        if (err.response == null) throw ConnectionFailedException();
        _checkAuthRedirect(err.response!);
      }
    }
    throw UnknownBaamApiException();
  }

  @override
  Future<void> stopSession(String sessionId) async {
    final String? token = await getToken();
    if (token == null) throw await _throwNotAuthorized();
    try {
      return await _api.stopSession(token, sessionId);
    } catch (err) {
      if (err is DioError) {
        if (err.response == null) throw ConnectionFailedException();
        _checkAuthRedirect(err.response!);
        if (err.response!.statusCode == 404) throw SessionNotFoundException(); // Active session not found
      }
    }
    throw UnknownBaamApiException();
  }

  @override
  Future<RunningSessionInfo> continueSession(String sessionId) async {
    final String? token = await getToken();
    if (token == null) throw await _throwNotAuthorized();
    try {
      await _api.continueSession(token, sessionId);
      throw UnknownBaamApiException();
    } catch (err) {
      if (err is DioError) {
        if (err.response == null) throw ConnectionFailedException();
        _checkAuthRedirect(err.response!);
        if (err.response!.statusCode == 404) throw SessionNotFoundException();
        if (err.response!.statusCode == 400) throw FailedToContinueSessionException();
        if (err.response!.statusCode == 302) {
          final sessionInfo = await getOrStartSession();
          if (sessionInfo.id != sessionId) throw UnknownBaamApiException();
          return sessionInfo;
        }
      }
    }
    throw UnknownBaamApiException();
  }

  @override
  Future<void> deleteSession(String sessionId) async {
    final String? token = await getToken();
    if (token == null) throw await _throwNotAuthorized();
    try {
      return await _api.deleteSession(token, sessionId);
    } catch (err) {
      if (err is DioError) {
        if (err.response == null) throw ConnectionFailedException();
        _checkAuthRedirect(err.response!);
        if (err.response!.statusCode == 404) throw SessionNotFoundException();
      }
    }
    throw UnknownBaamApiException();
  }

  @override
  Future<SessionInfo> getSessionInfo(String sessionId) async {
    final String? token = await getToken();
    if (token == null) throw await _throwNotAuthorized();
    try {
      return await _api.getSessionInfo(token, sessionId);
    } catch (err) {
      if (err is DioError) {
        if (err.response == null) throw ConnectionFailedException();
        _checkAuthRedirect(err.response!);
        if (err.response!.statusCode == 404) throw SessionNotFoundException();
      }
    }
    throw UnknownBaamApiException();
  }

  @override
  Future<void> changeSessionInfo(String sessionId, {required String title}) async {
    final String? token = await getToken();
    if (token == null) throw await _throwNotAuthorized();
    try {
      return await _api.changeSessionInfo(token, sessionId, title: title);
    } catch (err) {
      if (err is DioError) {
        if (err.response == null) throw ConnectionFailedException();
        _checkAuthRedirect(err.response!);
        if (err.response!.statusCode == 404) throw SessionNotFoundException();
      }
    }
    throw UnknownBaamApiException();
  }

  @override
  Future<void> addUserToSession(String sessionId, {required String userEmail}) async {
    final String? token = await getToken();
    if (token == null) throw await _throwNotAuthorized();
    try {
      return await _api.addUserToSession(token, sessionId, userEmail: userEmail);
    } catch (err) {
      if (err is DioError) {
        if (err.response == null) throw ConnectionFailedException();
        _checkAuthRedirect(err.response!);
        if (err.response!.statusCode == 404) throw SessionNotFoundException();
      }
    }
    throw UnknownBaamApiException();
  }

  @override
  Future<List<PartialSessionInfo>> getCollectedSessions() async {
    final String? token = await getToken();
    if (token == null) throw await _throwNotAuthorized();
    try {
      return await _api.getCollectedSessions(token);
    } catch (err) {
      if (err is DioError) {
        if (err.response == null) throw ConnectionFailedException();
        _checkAuthRedirect(err.response!);
      }
    }
    throw UnknownBaamApiException();
  }
}
