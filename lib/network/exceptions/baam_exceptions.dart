import "package:equatable/equatable.dart";

class BaamApiException extends Equatable implements Exception {
  @override
  List<Object> get props => [];
}

class UnknownBaamApiException extends BaamApiException {}

class ConnectionFailedException extends BaamApiException {}

class MalformedResponseException extends BaamApiException {}

class CodeIsUnacceptableException extends BaamApiException {}

class SessionNotFoundException extends BaamApiException {}

class FailedToContinueSessionException extends BaamApiException {}

class UserNotCheckedInForThisSessionException extends BaamApiException {}

class NotAuthorizedException extends BaamApiException {
  NotAuthorizedException(this.authUrl);
  final String authUrl;

  @override
  List<Object> get props => [authUrl];
}
