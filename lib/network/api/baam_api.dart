import "dart:convert";

import "package:baam/network/exceptions/baam_exceptions.dart";
import "package:baam/network/models/baam_models.dart";
import "package:dio/dio.dart";
import "package:html/parser.dart";
import "package:intl/intl.dart";

part "baam_api_impl.dart";

abstract class BaamApi {
  factory BaamApi(Dio dio, {String? baseUrl}) {
    return _BaamApi(dio, baseUrl: baseUrl ?? "https://baam.duckdns.org/");
  }

  Future<void> getRootPage();

  Future<UserInfo> getUserInfo(String token);

  Future<void> submitChallenge(String token, String sessionId, String code);

  Future<MarkedSessionInfo> getMarkedSessionInfo(String token, String sessionId);

  Future<RunningSessionInfo> getOrStartSession(String token);

  Future<void> stopSession(String token, String sessionId);

  Future<void> continueSession(String token, String sessionId);

  Future<void> deleteSession(String token, String sessionId);

  Future<SessionInfo> getSessionInfo(String token, String sessionId);

  Future<void> changeSessionInfo(String token, String sessionId, {required String title});

  Future<void> addUserToSession(String token, String sessionId, {required String userEmail});

  Future<List<PartialSessionInfo>> getCollectedSessions(String token);
}
