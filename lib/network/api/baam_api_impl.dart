part of "baam_api.dart";

class _BaamApi implements BaamApi {
  _BaamApi(this._dio, {required this.baseUrl});

  final Dio _dio;
  final String baseUrl;

  Future<Response<T>> _fetch<T>(String method, String path, String? token,
      {dynamic data, Map<String, dynamic>? queryParameters}) async {
    final Map<String, String> headers = {};
    if (token != null) {
      headers["Cookie"] = ".AspNetCore.Cookies=$token";
    }

    final Response<T> response = await _dio.fetch(Options(
      method: method,
      followRedirects: false,
      contentType: "application/json",
      headers: headers,
      validateStatus: (statusCode) {
        return statusCode == 200;
      },
    )
        .compose(_dio.options, path, data: data != null ? jsonEncode(data) : null, queryParameters: queryParameters)
        .copyWith(baseUrl: baseUrl));

    return response;
  }

  @override
  Future<void> getRootPage() async {
    await _fetch("GET", "/", null);
  }

  @override
  Future<UserInfo> getUserInfo(String token) async {
    final Response<String> res = await _fetch("GET", "/", token);
    final String document = res.data ?? "";
    int userNameStart = document.indexOf("const username = \"");
    if (userNameStart < 0) throw MalformedResponseException();
    userNameStart += "const username = \"".length;
    final userNameEnd = document.indexOf("\"", userNameStart);
    if (userNameEnd < userNameStart) throw MalformedResponseException();
    return UserInfo(email: document.substring(userNameStart, userNameEnd));
  }

  @override
  Future<void> submitChallenge(String token, String sessionId, String code) async {
    await _fetch("POST", "/api/AttendanceSession/$sessionId/submitChallenge", token, data: code);
  }

  @override
  Future<MarkedSessionInfo> getMarkedSessionInfo(String token, String sessionId) async {
    final Response<String> res = await _fetch("GET", "/MarkedSuccessfully/$sessionId", token);
    final String document = res.data ?? "";

    int sessionNameStart = document.indexOf("const session_name = \"");
    if (sessionNameStart < 0) throw MalformedResponseException();
    sessionNameStart += "const session_name = \"".length;
    final sessionNameEnd = document.indexOf("\";\n", sessionNameStart);
    if (sessionNameEnd < sessionNameStart) throw MalformedResponseException();

    int attendanceStart = document.indexOf("const attendance = [", sessionNameEnd);
    if (attendanceStart < 0) throw MalformedResponseException();
    attendanceStart += "const attendance = ".length;
    int attendanceEnd = document.indexOf("];\n", attendanceStart);
    if (attendanceEnd < attendanceStart) throw MalformedResponseException();
    attendanceEnd += 1;

    late dynamic attendance;
    try {
      attendance = jsonDecode(document.substring(attendanceStart, attendanceEnd));
    } catch (err) {
      throw MalformedResponseException();
    }

    if (attendance is! List) throw MalformedResponseException();
    List<UserInfo> users = attendance.map((e) {
      if (e is! Map) throw MalformedResponseException();
      if (!e.containsKey("item1")) throw MalformedResponseException();
      return UserInfo(email: e["item1"]);
    }).toList();

    return MarkedSessionInfo(sessionId, document.substring(sessionNameStart, sessionNameEnd), users);
  }

  @override
  Future<RunningSessionInfo> getOrStartSession(String token) async {
    final res = await _fetch("POST", "/api/AttendanceSession/getOrStart", token);
    if (res.data == null) throw MalformedResponseException();
    final sessionInfo = res.data!;

    if (sessionInfo is! Map) throw MalformedResponseException();
    if (!sessionInfo.containsKey("attendanceSeed") ||
        !sessionInfo.containsKey("initialIteration") ||
        !sessionInfo.containsKey("secretCodeIntervalMs") ||
        !sessionInfo.containsKey("sessionCode") ||
        !sessionInfo.containsKey("title")) throw MalformedResponseException();
    if (sessionInfo["attendanceSeed"] is! String ||
        sessionInfo["initialIteration"] is! int ||
        sessionInfo["secretCodeIntervalMs"] is! int ||
        sessionInfo["sessionCode"] is! String ||
        sessionInfo["title"] is! String) throw MalformedResponseException();
    return RunningSessionInfo(
      sessionInfo["sessionCode"],
      sessionInfo["title"],
      const <UserInfo>[],
      sessionInfo["attendanceSeed"],
      sessionInfo["secretCodeIntervalMs"],
      sessionInfo["initialIteration"],
    );
  }

  @override
  Future<void> stopSession(String token, String sessionId) async {
    await _fetch("POST", "/api/AttendanceSession/$sessionId/stop", token);
  }

  @override
  Future<void> continueSession(String token, String sessionId) async {
    await _fetch("POST", "/AddSessionStage/$sessionId", token);
  }

  @override
  Future<void> deleteSession(String token, String sessionId) async {
    await _fetch("DELETE", "/api/AttendanceSession/$sessionId", token);
  }

  @override
  Future<SessionInfo> getSessionInfo(String token, String sessionId) async {
    final res = await _fetch("GET", "/api/AttendanceSession/$sessionId", token);
    if (res.data == null) throw MalformedResponseException();
    final sessionInfo = res.data!;

    if (sessionInfo is! Map) throw MalformedResponseException();
    if (!sessionInfo.containsKey("attendanceMarks") ||
        !sessionInfo.containsKey("sessionCode") ||
        !sessionInfo.containsKey("title")) throw MalformedResponseException();
    final attendance = sessionInfo["attendanceMarks"];
    if (attendance is! List || sessionInfo["sessionCode"] is! String || sessionInfo["title"] is! String) {
      throw MalformedResponseException();
    }

    return SessionInfo(
      sessionInfo["sessionCode"],
      sessionInfo["title"],
      attendance.map((e) {
        if (e is! Map) throw MalformedResponseException();
        if (!e.containsKey("item1")) throw MalformedResponseException();
        return UserInfo(email: e["item1"]);
      }).toList(),
    );
  }

  @override
  Future<void> changeSessionInfo(String token, String sessionId, {required String title}) async {
    await _fetch("PUT", "/api/AttendanceSession/$sessionId", token, data: {"title": title});
  }

  @override
  Future<void> addUserToSession(String token, String sessionId, {required String userEmail}) async {
    await _fetch("POST", "/api/AttendanceSession/$sessionId/addStudent", token, data: userEmail);
  }

  @override
  Future<List<PartialSessionInfo>> getCollectedSessions(String token) async {
    final Response<String> res = await _fetch("GET", "/AttendanceSessions", token);
    final document = parse(res.data ?? "");

    var table = document.querySelector(".container > table");
    if (table == null) throw MalformedResponseException();
    var rows = table.querySelectorAll("tr");
    var result = <PartialSessionInfo>[];
    for (var row in rows) {
      var cells = row.querySelectorAll("td");
      if (cells.isEmpty) continue;
      if (cells.length != 5) throw MalformedResponseException();

      var sessionId = cells[4].querySelector("a")?.attributes["data-code"];
      DateTime date;
      try {
        date = DateFormat("dd.MM.yyyy hh:mm:ss").parse(cells[0].querySelector("a")?.innerHtml ?? "");
      } on FormatException {
        throw MalformedResponseException();
      }

      var title = cells[1].querySelector("a")?.innerHtml;
      var userCount = int.tryParse(cells[2].querySelector("a")?.innerHtml ?? "");

      if (title == null || sessionId == null || userCount == null) throw MalformedResponseException();
      result.add(PartialSessionInfo(sessionId, title, date, userCount));
    }

    return result;
  }
}
