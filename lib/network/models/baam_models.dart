import "dart:convert";
import "package:crypto/crypto.dart";
import "package:equatable/equatable.dart";

class UserInfo extends Equatable {
  const UserInfo({required this.email});
  final String email;

  @override
  List<Object> get props => [email];
}

class PartialSessionInfo extends Equatable {
  const PartialSessionInfo(this.id, this.title, this.date, this.userCount);
  final String id;
  final String title;
  final DateTime date;
  final int userCount;

  @override
  List<Object> get props => [id, title, date, userCount];
}

class SessionInfo extends Equatable {
  const SessionInfo(this.id, this.title, this.users);
  final String id;
  final String title;
  final List<UserInfo> users;

  @override
  List<Object> get props => [id, title, users];
}

class RunningSessionInfo extends SessionInfo {
  const RunningSessionInfo(String id, String title, List<UserInfo> users, this.attendanceSeed,
      this.secretCodeIntervalMs, this.initialIteration)
      : super(id, title, users);
  final String attendanceSeed;
  final int secretCodeIntervalMs;
  final int initialIteration;

  @override
  List<Object> get props => [id, title, users, attendanceSeed, secretCodeIntervalMs, initialIteration];

  String getAttendanceCode(int index) {
    List<int> data = [...attendanceSeed.codeUnits];
    for (int i = 0; i < 4; i++) {
      data.add(index >> (8 * i) & 0xff);
    }
    String base64hash = base64UrlEncode(sha1.convert(data).bytes.take(6).toList());
    return base64hash.replaceAll("-", "+") + index.toRadixString(36);
  }

  RunningSessionInfo updateSessionInfo(SessionInfo info) {
    return RunningSessionInfo(info.id, info.title, info.users, attendanceSeed, secretCodeIntervalMs, initialIteration);
  }
}

class MarkedSessionInfo {
  const MarkedSessionInfo(this.id, this.title, this.users);
  final String id;
  final String title;
  final List<UserInfo> users;

  List<String> get emails => users.map((e) => e.email).toList();
}
