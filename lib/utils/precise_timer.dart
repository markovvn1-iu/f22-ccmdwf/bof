class PreciseTimer {
  PreciseTimer(Duration duration, this.callback)
      : startTime = DateTime.now().millisecondsSinceEpoch,
        duration = duration.inMilliseconds {
    _update();
  }

  bool isWorking = true;
  int startTime;
  int duration;
  void Function() callback;

  int count() {
    int now = DateTime.now().millisecondsSinceEpoch;
    double delta = (now - startTime) / duration;
    return delta.round();
  }

  void _update() async {
    while (isWorking) {
      callback();

      final int now = DateTime.now().millisecondsSinceEpoch;
      double delta = (now - startTime) / duration;
      delta -= delta.round();
      await Future.delayed(Duration(milliseconds: ((1 - delta) * duration).round()));
    }
  }

  void stop() {
    isWorking = false;
  }
}
