import "package:baam/network/api/baam_api.dart";
import "package:baam/network/repository/baam_repository.dart";
import "package:dio/dio.dart";
import "package:get_it/get_it.dart";

final GetIt locator = GetIt.instance;

void initLocator() {
  Dio dio = Dio(BaseOptions(
    connectTimeout: 1000 * 15,
    receiveTimeout: 1000 * 15,
    sendTimeout: 1000 * 15,
  ));

  locator.registerSingleton(dio);

  locator.registerLazySingleton<BaamApi>(() => BaamApi(locator.get<Dio>()));
  locator.registerLazySingleton<BaamRepository>(() => BaamRepository(locator.get<BaamApi>()));
}
