import "package:baam/network/models/baam_models.dart";
import "package:flutter/material.dart";

class SessionUsersList extends StatelessWidget {
  const SessionUsersList({super.key, required this.users});

  final List<UserInfo> users;

  @override
  Widget build(BuildContext context) {
    const listBorder = BorderSide(
      width: 1,
      color: Color.fromRGBO(0, 0, 0, 0.2),
    );

    return Container(
      decoration: const BoxDecoration(
        border: Border.fromBorderSide(listBorder),
      ),
      child: ListView.builder(
        // Makes the ScrollView stick to the bottom edge
        reverse: true,
        padding: EdgeInsets.zero,
        itemCount: users.length,
        itemBuilder: (context, reverseIndex) {
          final index = users.length - 1 - reverseIndex;
          return Container(
            decoration: const BoxDecoration(
              border: Border(
                top: listBorder,
              ),
            ),
            child: Wrap(
              spacing: 10,
              crossAxisAlignment: WrapCrossAlignment.center,
              children: [
                Container(
                  constraints: const BoxConstraints.expand(
                    width: 35,
                    height: 30,
                  ),
                  color: const Color.fromRGBO(0, 0, 0, 0.1),
                  child: Center(
                    child: Text("${index + 1}"),
                  ),
                ),
                Text(users[index].email),
              ],
            ),
          );
        },
      ),
    );
  }
}
