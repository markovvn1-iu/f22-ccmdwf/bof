import "package:flutter/material.dart";

class SingleChildExpandedScrollView extends StatelessWidget {
  const SingleChildExpandedScrollView({super.key, required this.child});

  final Widget child;

  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      physics: const AlwaysScrollableScrollPhysics(),
      slivers: [
        SliverFillRemaining(child: child),
      ],
    );
  }
}
