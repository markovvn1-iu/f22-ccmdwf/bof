import "package:baam/bloc/checkin/checkin_bloc.dart";
import "package:baam/bloc/checkin/exceptions.dart";
import "package:baam/locator.dart";
import "package:baam/network/exceptions/baam_exceptions.dart";
import "package:baam/network/repository/baam_repository.dart";
import "package:baam/screens/checkin/widgets/animated_check_icon.dart";
import "package:flutter/material.dart";
import "package:flutter_bloc/flutter_bloc.dart";
import "package:fluttertoast/fluttertoast.dart";
import "package:mobile_scanner/mobile_scanner.dart";
import "package:flutter_gen/gen_l10n/app_localizations.dart";

class CheckInScreen extends StatefulWidget {
  const CheckInScreen({super.key});

  @override
  CheckInScreenState createState() => CheckInScreenState();
}

class CheckInScreenState extends State<CheckInScreen> with SingleTickerProviderStateMixin {
  late AnimationController controller;

  @override
  void initState() {
    super.initState();
    controller = AnimationController(duration: const Duration(milliseconds: 800), vsync: this);
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  Widget _buildCheckedInBody(BuildContext context, CheckedInState state) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Padding(padding: const EdgeInsets.all(20), child: AnimatedCheckIcon(listenable: controller)),
          Padding(
            padding: const EdgeInsets.all(20),
            child: Text(AppLocalizations.of(context)!.successmsg + state.info.title),
          ),
          Container(
            padding: const EdgeInsets.all(30),
            child: ListView.separated(
              scrollDirection: Axis.vertical,
              shrinkWrap: true,
              itemCount: state.info.emails.length,
              itemBuilder: (_, i) => Text(
                state.info.emails[i],
              ),
              separatorBuilder: (_, __) => const Divider(),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildScanningBody(BuildContext context, ScanningState state) {
    return Center(
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(
              top: 30,
              bottom: 40,
            ),
            child: Text(AppLocalizations.of(context)!.checkinhint),
          ),
          SizedBox(
            height: 400,
            width: 300,
            child: MobileScanner(
              allowDuplicates: false,
              fit: BoxFit.scaleDown,
              controller: MobileScannerController(
                facing: CameraFacing.back,
                torchEnabled: false,
              ),
              onDetect: (barcode, _) {
                if (barcode.rawValue == null) return;
                context.read<CheckInBloc>().add(ParseAndSubmitCodeEvent(barcode.rawValue!));
              },
            ),
          ),
        ],
      ),
    );
  }

  void showExceptionToast(Exception e) {
    final String msg;
    if (e is MalformedQRCode) {
      msg = AppLocalizations.of(context)!.malformedqr;
    } else if (e is ConnectionFailedException) {
      msg = AppLocalizations.of(context)!.connectionfailed;
    } else if (e is CodeIsUnacceptableException) {
      msg = AppLocalizations.of(context)!.codeunacceptable;
    } else if (e is SessionNotFoundException) {
      msg = AppLocalizations.of(context)!.sessionnotfound;
    } else {
      msg = AppLocalizations.of(context)!.unknownerror;
    }
    Fluttertoast.showToast(
      msg: msg,
      backgroundColor: Colors.yellow,
      gravity: ToastGravity.BOTTOM,
      toastLength: Toast.LENGTH_SHORT,
    );
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
        create: (_) => CheckInBloc(locator<BaamRepository>()),
        child: BlocListener<CheckInBloc, CheckInState>(
          listener: (context, state) {
            if (state is CheckedInState) {
              controller.reset();
              controller.forward();
            } else if (state is ScanningState) {
              if (state.exception == null) return;
              showExceptionToast(state.exception!);
            }
          },
          child: Scaffold(
            appBar: AppBar(
              backgroundColor: Colors.green,
              title: Text(AppLocalizations.of(context)!.checkin),
              leading: IconButton(
                icon: const Icon(Icons.arrow_back),
                onPressed: () => Navigator.pop(context),
              ),
            ),
            body: SafeArea(
              child: BlocBuilder<CheckInBloc, CheckInState>(builder: (context, state) {
                if (state is ScanningState) {
                  return _buildScanningBody(context, state);
                } else if (state is CheckedInState) {
                  return _buildCheckedInBody(context, state);
                }
                throw Exception("Unknown state");
              }),
            ),
          ),
        ));
  }
}
