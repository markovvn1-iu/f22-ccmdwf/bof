import "package:flutter/material.dart";

class AnimatedCheckIcon extends AnimatedWidget {
  const AnimatedCheckIcon({
    super.key,
    required Animation<double> super.listenable,
  });

  @override
  Widget build(BuildContext context) {
    final animation = listenable as Animation<double>;
    return Stack(
      alignment: AlignmentDirectional.center,
      children: [
        const Icon(
          Icons.circle_outlined,
          size: 150,
          color: Colors.green,
        ),
        ShaderMask(
          blendMode: BlendMode.dstIn,
          shaderCallback: (bounds) => LinearGradient(
            colors: const [Colors.white, Colors.white, Colors.transparent, Colors.transparent],
            stops: [0, animation.value, (animation.value + 0.1).clamp(0, 1), 1],
          ).createShader(bounds),
          child: const Icon(
            Icons.check,
            size: 100,
            color: Colors.green,
          ),
        ),
      ],
    );
  }
}
