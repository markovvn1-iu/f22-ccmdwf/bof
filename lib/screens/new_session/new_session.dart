import "package:baam/bloc/new_session/new_session_bloc.dart";
import "package:baam/network/repository/baam_repository.dart";
import "package:baam/screens/new_session/widgets/delete_session_button.dart";
import "package:baam/screens/new_session/widgets/save_session_button.dart";
import "package:baam/screens/new_session/widgets/session_name_field.dart";
import "package:baam/screens/new_session/widgets/user_add_field.dart";
import "package:baam/locator.dart";
import "package:baam/network/exceptions/baam_exceptions.dart";
import "package:baam/network/models/baam_models.dart";
import "package:baam/widgets/session_users_list.dart";
import "package:flutter/material.dart";
import "package:flutter_bloc/flutter_bloc.dart";
import "package:fluttertoast/fluttertoast.dart";
import "package:flutter_gen/gen_l10n/app_localizations.dart";
import "package:qr_flutter/qr_flutter.dart";

class NewSessionScreen extends StatelessWidget {
  NewSessionScreen({super.key});

  final _userAddFieldController = TextEditingController();
  final _sessionNameFieldController = TextEditingController();
  final _sessionNameFieldFocusNode = FocusNode();

  Widget _buildBody(BuildContext context, NewSessionState state) {
    final AppLocalizations localizations = AppLocalizations.of(context)!;

    if (state is InitialState || state is LoadingState) {
      return Center(
        child: Column(mainAxisSize: MainAxisSize.min, children: [
          const Padding(padding: EdgeInsets.all(20), child: CircularProgressIndicator()),
          Text(
            localizations.loading,
            style: Theme.of(context).textTheme.headlineSmall,
          )
        ]),
      );
    }

    if (state is RunningSessionState || state is StopSessionLoadingState || state is StoppedSessionState) {
      final SessionInfo info;
      if (state is RunningSessionState) {
        info = state.info;
      } else if (state is StopSessionLoadingState) {
        info = state.info;
      } else {
        info = (state as StoppedSessionState).info;
      }

      bool disableUI = state is StopSessionLoadingState;
      return Padding(
        padding: const EdgeInsets.all(20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          mainAxisSize: MainAxisSize.max,
          children: [
            SessionNameField(
              enabled: !disableUI,
              controller: _sessionNameFieldController,
              focusNode: _sessionNameFieldFocusNode,
              onRename: () => context.read<NewSessionBloc>().add(
                    RenameSessionEvent(_sessionNameFieldController.text),
                  ),
            ),
            if (state is RunningSessionState)
              Flexible(
                flex: 3,
                child: Container(
                  color: Colors.white,
                  child: QrImage(errorCorrectionLevel: QrErrorCorrectLevel.L, data: state.checkInUrl),
                ),
              ),
            Flexible(
              child: Padding(
                padding: const EdgeInsets.only(top: 20, bottom: 10),
                child: SessionUsersList(
                  users: info.users,
                ),
              ),
            ),
            UserAddField(
              enabled: !disableUI,
              controller: _userAddFieldController,
              onUserAdded: () {
                context.read<NewSessionBloc>().add(AddUserEvent(_userAddFieldController.text));
                _userAddFieldController.clear();
              },
            ),
          ],
        ),
      );
    }

    return Center(
      child: Text(
        localizations.somethingwentwrong,
        style: Theme.of(context).textTheme.headlineSmall,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) {
        final bloc = NewSessionBloc(locator<BaamRepository>());
        bloc.add(GetOrStartSessionEvent());
        return bloc;
      },
      child: MultiBlocListener(
        listeners: [
          BlocListener<NewSessionBloc, NewSessionState>(
            listener: (context, state) {
              if (state is! RunningSessionState && state is! StoppedSessionState) return;
              SessionInfo info = (state is RunningSessionState) ? state.info : (state as StoppedSessionState).info;

              Fluttertoast.showToast(
                msg: AppLocalizations.of(context)!.sessionnameupdated,
                backgroundColor: Colors.lightGreen,
                gravity: ToastGravity.TOP,
                toastLength: Toast.LENGTH_SHORT,
              );
              _sessionNameFieldController.text = info.title; // Load session name
            },
            listenWhen: (previous, current) {
              bool previousOk = (previous is RunningSessionState) || (previous is StoppedSessionState);
              bool currentOk = (current is RunningSessionState) || (current is StoppedSessionState);
              if (!previousOk || !currentOk) return false;
              SessionInfo previousInfo =
                  (previous is RunningSessionState) ? previous.info : (previous as StoppedSessionState).info;
              SessionInfo currentInfo =
                  (current is RunningSessionState) ? current.info : (current as StoppedSessionState).info;
              return previousInfo.title != currentInfo.title;
            },
          ),
          BlocListener<NewSessionBloc, NewSessionState>(
            listener: (context, state) {
              if (state is! RunningSessionState) return;
              _sessionNameFieldController.text = state.info.title; // Load session name
            },
            listenWhen: (previous, current) {
              return (current is RunningSessionState && previous is! RunningSessionState);
            },
          ),
          BlocListener<NewSessionBloc, NewSessionState>(
            listener: (context, state) {
              final snackbar = state.isConnectionStable
                  ? SnackBar(
                      content: Text(AppLocalizations.of(context)!.connectionrestored),
                      behavior: SnackBarBehavior.floating,
                      backgroundColor: Colors.lightGreen,
                      duration: const Duration(seconds: 1),
                    )
                  : SnackBar(
                      content: Text(AppLocalizations.of(context)!.connectionfailed),
                      behavior: SnackBarBehavior.floating,
                      backgroundColor: Colors.red,
                      duration: const Duration(seconds: 5),
                    );

              ScaffoldMessenger.of(context).showSnackBar(snackbar);
            },
            listenWhen: (previous, current) {
              return previous.isConnectionStable != current.isConnectionStable;
            },
          ),
          BlocListener<NewSessionBloc, NewSessionState>(
            listener: (context, state) {
              if (state.exception == null) return;
              String msg;
              if (state.exception is ConnectionFailedException) {
                msg = AppLocalizations.of(context)!.connectionfailed;
              } else if (state.exception is SessionNotFoundException) {
                msg = AppLocalizations.of(context)!.sessionnotfound;
              } else {
                msg = "Unknown error";
              }

              ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                content: Text(msg),
                behavior: SnackBarBehavior.floating,
                backgroundColor: Colors.yellow[700],
                duration: const Duration(seconds: 5),
              ));
            },
          ),
        ],
        child: BlocBuilder<NewSessionBloc, NewSessionState>(builder: (context, state) {
          return Scaffold(
            appBar: AppBar(
              title: Text(AppLocalizations.of(context)!.newsession),
              leading: IconButton(
                icon: const Icon(Icons.arrow_back),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
              backgroundColor: Colors.blue,
              actions: [
                if (state is RunningSessionState)
                  SaveSessionButton(
                    onSave: () => context.read<NewSessionBloc>().add(StopSessionEvent()),
                  ),
                if (state is RunningSessionState)
                  DeleteSessionButton(
                    onDelete: () => context.read<NewSessionBloc>().add(ResetSessionEvent()),
                  ),
              ],
            ),
            body: SafeArea(child: _buildBody(context, state)),
          );
        }),
      ),
    );
  }
}
