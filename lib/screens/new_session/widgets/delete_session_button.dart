import "package:baam/screens/new_session/widgets/delete_dialog.dart";
import "package:flutter/material.dart";

class DeleteSessionButton extends StatelessWidget {
  const DeleteSessionButton({super.key, required this.onDelete});

  final VoidCallback onDelete;

  @override
  Widget build(BuildContext context) {
    return IconButton(
      onPressed: () {
        showDialog(
          context: context,
          builder: (context) => DeleteDialog(
            onDelete: onDelete,
          ),
        );
      },
      icon: const Icon(Icons.delete_rounded),
    );
  }
}
