import "package:flutter/material.dart";
import "package:flutter_gen/gen_l10n/app_localizations.dart";

class UserAddField extends StatelessWidget {
  const UserAddField({super.key, required this.onUserAdded, required this.controller, this.enabled = true});

  final bool enabled;
  final VoidCallback onUserAdded;
  final TextEditingController controller;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      enabled: enabled,
      controller: controller,
      keyboardType: TextInputType.emailAddress,
      textAlignVertical: TextAlignVertical.bottom,
      decoration: InputDecoration(
        isDense: true,
        border: const OutlineInputBorder(),
        labelText: AppLocalizations.of(context)!.addmanually,
        hintText: "n.surname@innopolis.university",
        suffixIcon: IconButton(
          icon: const Icon(Icons.add_rounded),
          onPressed: onUserAdded,
        ),
      ),
      onFieldSubmitted: (_) => onUserAdded(),
    );
  }
}
