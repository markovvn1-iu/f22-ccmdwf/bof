import "package:flutter/material.dart";

class SaveSessionButton extends StatelessWidget {
  const SaveSessionButton({super.key, required this.onSave});

  final VoidCallback onSave;

  @override
  Widget build(BuildContext context) {
    return IconButton(
      onPressed: onSave,
      icon: const Icon(Icons.save_rounded),
    );
  }
}
