import "package:flutter/material.dart";
import "package:flutter_gen/gen_l10n/app_localizations.dart";

class SessionNameField extends StatelessWidget {
  SessionNameField({
    super.key,
    required this.onRename,
    required this.controller,
    required this.focusNode,
    this.enabled = true,
  }) {
    focusNode.addListener(
      () {
        if (!focusNode.hasFocus) {
          return;
        }
        final textLength = controller.text.length;
        controller.selection = TextSelection(
          baseOffset: 0,
          extentOffset: textLength,
        );
      },
    );
  }

  final bool enabled;
  final TextEditingController controller;
  final VoidCallback onRename;
  final FocusNode focusNode;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      enabled: enabled,
      controller: controller,
      textAlignVertical: TextAlignVertical.bottom,
      decoration: InputDecoration(
        isDense: true,
        border: const OutlineInputBorder(),
        labelText: AppLocalizations.of(context)!.sessionname,
        hintText: AppLocalizations.of(context)!.untitledsession,
        suffixIcon: IconButton(
          icon: const Icon(Icons.done),
          onPressed: onRename,
        ),
      ),
      focusNode: focusNode,
      onFieldSubmitted: (_) => onRename(),
    );
  }
}
