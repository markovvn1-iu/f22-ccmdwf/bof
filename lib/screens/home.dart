import "package:baam/bloc/home/home_bloc.dart";
import "package:baam/locator.dart";
import "package:baam/network/models/baam_models.dart";
import "package:baam/network/repository/baam_repository.dart";
import "package:baam/routes.dart";
import "package:baam/screens/auth.dart";
import "package:flutter/material.dart";
import "package:flutter_bloc/flutter_bloc.dart";
import "package:flutter_gen/gen_l10n/app_localizations.dart";

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  Widget actionButton({
    required BuildContext? context,
    required IconData iconData,
    required String text,
    required Color buttonBackgroundColor,
    required Function()? onPressed,
  }) {
    return ElevatedButton(
      onPressed: onPressed,
      style: ElevatedButton.styleFrom(
        backgroundColor: buttonBackgroundColor,
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.all(10),
            child: Icon(
              iconData,
              size: 50,
            ),
          ),
          Text(
            text,
            style: const TextStyle(
              fontSize: 20,
            ),
            textAlign: TextAlign.center,
          ),
        ],
      ),
    );
  }

  Widget _buildLogoutDialog(BuildContext context) {
    return AlertDialog(
      title: Text(AppLocalizations.of(context)!.logout),
      content: Text(AppLocalizations.of(context)!.logoutwarning),
      actions: [
        ElevatedButton(
          style: ElevatedButton.styleFrom(
            backgroundColor: Colors.red,
          ),
          child: Text(AppLocalizations.of(context)!.logout),
          onPressed: () {
            context.read<HomeBloc>().add(LogoutEvent());
            Navigator.pop(context);
          },
        ),
        ElevatedButton(
          child: Text(AppLocalizations.of(context)!.cancel),
          onPressed: () => Navigator.pop(context),
        ),
      ],
    );
  }

  Future showLogoutDialog(BuildContext superContext) {
    return showDialog(
      context: superContext,
      builder: (BuildContext context) {
        return BlocProvider.value(
          value: superContext.read<HomeBloc>(),
          child: BlocBuilder<HomeBloc, HomeState>(builder: (context, state) => _buildLogoutDialog(context)),
        );
      },
    );
  }

  Widget _buildDrawer(BuildContext context) {
    return Drawer(
      width: 250,
      child: ListView(
        children: [
          DrawerHeader(
            child: Center(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  const Image(
                    image: AssetImage("assets/images/drawer_icon.png"),
                    height: 100,
                    width: 100,
                  ),
                  Text(AppLocalizations.of(context)!.baam),
                ],
              ),
            ),
          ),
          ListTile(
            title: Text(AppLocalizations.of(context)!.newsession),
            leading: const Icon(Icons.add_circle_outline_rounded),
            onTap: () => Navigator.pushNamed(context, Routes.newSession),
          ),
          ListTile(
            title: Text(AppLocalizations.of(context)!.checkin),
            leading: const Icon(Icons.check_circle_rounded),
            onTap: () => Navigator.pushNamed(context, Routes.checkIn),
          ),
          ListTile(
            title: Text(AppLocalizations.of(context)!.collectedsessions),
            leading: const Icon(Icons.list_rounded),
            onTap: () => Navigator.pushNamed(context, Routes.collectedSessions),
          ),
          ListTile(
            title: Text(AppLocalizations.of(context)!.logout),
            leading: const Icon(Icons.exit_to_app),
            onTap: () => showLogoutDialog(context),
          ),
        ],
      ),
    );
  }

  Widget _buildBody(BuildContext context, HomeState state) {
    final UserInfo? userInfo = state is AuthorizedState ? state.userInfo : null;

    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      mainAxisSize: MainAxisSize.min,
      children: [
        Flexible(
          flex: 1,
          child: Padding(
            padding: const EdgeInsets.all(10),
            child: ListTile(
              title: Text(userInfo?.email ?? ""),
              leading: const Icon(Icons.person_rounded),
              shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(10)),
              ),
              tileColor: const Color(0xD3D3D3D3),
            ),
          ),
        ),
        Flexible(
          flex: 4,
          child: Padding(
            padding: const EdgeInsets.all(10),
            child: GridView.count(
              physics: const NeverScrollableScrollPhysics(),
              crossAxisCount: 2,
              mainAxisSpacing: 20,
              crossAxisSpacing: 20,
              children: [
                actionButton(
                  context: context,
                  iconData: Icons.add_circle_outline_rounded,
                  buttonBackgroundColor: Colors.blue,
                  text: AppLocalizations.of(context)!.newsession,
                  onPressed: () => Navigator.pushNamed(context, Routes.newSession),
                ),
                actionButton(
                  context: context,
                  iconData: Icons.check_circle_outline_rounded,
                  buttonBackgroundColor: Colors.green,
                  text: AppLocalizations.of(context)!.checkin,
                  onPressed: () => Navigator.pushNamed(context, Routes.checkIn),
                ),
                actionButton(
                  context: context,
                  iconData: Icons.list,
                  buttonBackgroundColor: Colors.orange,
                  text: AppLocalizations.of(context)!.collectedsessions,
                  onPressed: () => Navigator.pushNamed(context, Routes.collectedSessions),
                ),
                actionButton(
                  context: context,
                  buttonBackgroundColor: Colors.red,
                  iconData: Icons.exit_to_app_rounded,
                  text: AppLocalizations.of(context)!.logout,
                  onPressed: () => showLogoutDialog(context),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
        create: (_) {
          final bloc = HomeBloc(locator<BaamRepository>());
          bloc.add(CheckAuthEvent());
          return bloc;
        },
        child: BlocConsumer<HomeBloc, HomeState>(
          listener: (context, state) {
            if (state is NotAuthorizedState) {
              AuthScreen.showAuthDialog(context, state.authUrl)
                  .then((token) => context.read<HomeBloc>().add(token != null ? LoginEvent(token) : CheckAuthEvent()));
            }
          },
          builder: (context, state) {
            return Scaffold(
              appBar: AppBar(
                backgroundColor: Colors.blue,
                title: Text(AppLocalizations.of(context)!.baam),
              ),
              drawer: _buildDrawer(context),
              body: SafeArea(
                child: _buildBody(context, state),
              ),
            );
          },
        ));
  }
}
