import "dart:async";

import "package:flutter/material.dart";
import "package:flutter_inappwebview/flutter_inappwebview.dart";

class AuthScreen extends StatefulWidget {
  const AuthScreen(this._authUrl, {super.key});

  final String _authUrl;

  static Future<String?> showAuthDialog(BuildContext context, String authUrl) {
    return showDialog<String>(context: context, builder: (context) => AuthScreen(authUrl));
  }

  @override
  AuthScreenState createState() => AuthScreenState();
}

class AuthScreenState extends State<AuthScreen> {
  String? token;

  @override
  Widget build(BuildContext context) {
    if (token != null) {
      Navigator.of(context).pop(token);
    }
    return Scaffold(
        body: InAppWebView(
            initialOptions: InAppWebViewGroupOptions(
                crossPlatform: InAppWebViewOptions(
              useShouldOverrideUrlLoading: true,
            )),
            initialUrlRequest: URLRequest(url: Uri.parse(widget._authUrl)),
            shouldOverrideUrlLoading: (controller, navigationAction) async {
              Uri uri = navigationAction.request.url!;
              if (uri.host != "baam.duckdns.org") return NavigationActionPolicy.ALLOW;
              CookieManager cookieManager = CookieManager.instance();

              Cookie? value = await cookieManager.getCookie(url: uri, name: ".AspNetCore.Cookies");
              if (value == null) return NavigationActionPolicy.ALLOW;
              setState(() {
                token = value.value;
              });
              Uri innopolisSSO = Uri.parse("https://sso.university.innopolis.ru/adfs/");
              unawaited(cookieManager.deleteCookies(url: innopolisSSO, path: "/adfs"));
              return NavigationActionPolicy.CANCEL;
            }));
  }
}
