import "package:baam/network/models/baam_models.dart";
import "package:flutter/material.dart";
import "package:intl/intl.dart";
import "package:flutter_gen/gen_l10n/app_localizations.dart";

class SessionRow extends StatelessWidget {
  const SessionRow({
    super.key,
    required this.sessionInfo,
    required this.onDelete,
  });

  final PartialSessionInfo sessionInfo;
  final VoidCallback onDelete;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      contentPadding: const EdgeInsets.only(left: 20, right: 10, top: 10),
      title: Text(sessionInfo.title),
      subtitle: Text(DateFormat("dd.MM.yyyy HH:mm:ss").format(sessionInfo.date)),
      leading: Stack(
        children: [
          const Padding(
            padding: EdgeInsets.only(
              top: 8,
              right: 8,
              left: 8,
            ),
            child: Icon(
              Icons.people_rounded,
              size: 35,
            ),
          ),
          Positioned(
            top: 0,
            right: 0,
            child: Container(
              padding: const EdgeInsets.all(5),
              decoration: BoxDecoration(
                color: Colors.orange,
                borderRadius: BorderRadius.circular(30),
              ),
              constraints: const BoxConstraints(
                minWidth: 12,
                minHeight: 12,
              ),
              child: Text(
                sessionInfo.userCount.toString(),
                style: const TextStyle(
                  color: Colors.white,
                  fontSize: 10,
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ),
        ],
      ),
      trailing: IconButton(
        icon: const Icon(Icons.delete_rounded),
        onPressed: () {
          showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Text(AppLocalizations.of(context)!.deletesession),
                content: Text(AppLocalizations.of(context)!.areyousuredeletethis),
                actions: [
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.red,
                    ),
                    onPressed: onDelete,
                    child: Text(AppLocalizations.of(context)!.delete),
                  ),
                  ElevatedButton(
                    child: Text(AppLocalizations.of(context)!.cancel),
                    onPressed: () => Navigator.pop(context),
                  ),
                ],
              );
            },
          );
        },
      ),
    );
  }
}
