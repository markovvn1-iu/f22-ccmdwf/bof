import "package:baam/bloc/collected_sessions/session_list_bloc.dart";
import "package:baam/locator.dart";
import "package:baam/network/repository/baam_repository.dart";
import "package:baam/screens/collected_sessions/widgets/session_row.dart";
import "package:baam/widgets/single_child_expanded_scroll_view.dart";
import "package:flutter/material.dart";
import "package:flutter_bloc/flutter_bloc.dart";
import "package:flutter_gen/gen_l10n/app_localizations.dart";

class CollectedSessionsScreen extends StatelessWidget {
  const CollectedSessionsScreen({super.key});

  PreferredSizeWidget appBar(BuildContext context) {
    return AppBar(
      title: Text(AppLocalizations.of(context)!.collectedsessions),
      leading: IconButton(
        icon: const Icon(Icons.arrow_back),
        onPressed: () {
          Navigator.pop(context);
        },
      ),
      backgroundColor: Colors.orange,
    );
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) {
        final bloc = SessionListBloc(locator<BaamRepository>());
        bloc.add(SessionListLoadEvent());
        return bloc;
      },
      child: MultiBlocListener(
        listeners: [
          BlocListener<SessionListBloc, SessionListState>(
            listener: (context, state) {
              if (state is SessionListInitial) {
                context.read<SessionListBloc>().add(SessionListLoadEvent());
              }
            },
          ),
          BlocListener<SessionListBloc, SessionListState>(
            listener: (context, state) {
              if (state is! SessionDeletedState) {
                return;
              }

              ScaffoldMessenger.of(context)
                ..clearSnackBars()
                ..showSnackBar(
                  SnackBar(
                    content: Text(AppLocalizations.of(context)!.sessionDeletedSuccessfully),
                    duration: const Duration(seconds: 1),
                  ),
                );
              context.read<SessionListBloc>().add(WidgetNotifiedEvent());
            },
          ),
        ],
        child: BlocBuilder<SessionListBloc, SessionListState>(
          builder: (context, state) {
            final Widget body;
            if (state is SessionListInitial) {
              body = SingleChildExpandedScrollView(
                child: Center(
                  child: Text(
                    AppLocalizations.of(context)!.loading,
                    style: Theme.of(context).textTheme.headlineMedium,
                  ),
                ),
              );
            } else if (state is SessionListLoadedState) {
              var sessions = context.read<SessionListBloc>().sessions;
              body = ListView.builder(
                itemCount: sessions.length,
                itemBuilder: (context, index) => SessionRow(
                  sessionInfo: sessions[index],
                  onDelete: () {
                    context.read<SessionListBloc>().add(SessionListDeleteEvent(sessions[index]));
                    Navigator.of(context).pop();
                  },
                ),
              );
            } else if (state is ConnectionFailedState) {
              body = SingleChildExpandedScrollView(
                child: Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      const Icon(
                        Icons.signal_wifi_statusbar_connected_no_internet_4,
                        size: 100,
                      ),
                      Text(
                        AppLocalizations.of(context)!.connectionfailed,
                        style: Theme.of(context).textTheme.headlineSmall,
                      ),
                    ],
                  ),
                ),
              );
            } else {
              body = Center(
                child: Text(
                  AppLocalizations.of(context)!.somethingwentwrong,
                  style: Theme.of(context).textTheme.headlineSmall,
                ),
              );
            }

            return Scaffold(
              appBar: appBar(context),
              body: RefreshIndicator(
                onRefresh: () async => context.read<SessionListBloc>().add(SessionListReloadEvent()),
                child: body,
              ),
            );
          },
        ),
      ),
    );
  }
}
