class Routes {
  static const home = "/";
  static const newSession = "/newSession";
  static const checkIn = "/checkIn";
  static const collectedSessions = "/collectedSessions";
}
