import "package:baam/bloc/checkin/checkin_bloc.dart";
import "package:baam/bloc/checkin/exceptions.dart";
import "package:baam/bloc/collected_sessions/session_list_bloc.dart";
import "package:baam/bloc/home/home_bloc.dart" hide InitialState;
import "package:baam/bloc/new_session/new_session_bloc.dart";
import "package:baam/network/exceptions/baam_exceptions.dart";
import "package:baam/network/models/baam_models.dart";
import "package:baam/network/repository/baam_repository.dart";
import "package:flutter_test/flutter_test.dart";
import "package:mockito/annotations.dart";
import "package:mockito/mockito.dart";

import "states_test.mocks.dart";

@GenerateMocks([BaamRepository])
void main() {
  late MockBaamRepository repository;

  setUp(() {
    repository = MockBaamRepository();
  });

  group("home bloc", () {
    late HomeBloc homeBloc;

    setUp(() {
      homeBloc = HomeBloc(repository);
    });

    test("not being logged in results in the corresponding state", () {
      when(repository.getUserInfo()).thenThrow(NotAuthorizedException("some_redirect"));

      homeBloc.add(CheckAuthEvent());
      expect(homeBloc.stream, emits(NotAuthorizedState("some_redirect")));
    });

    test("being logged in results in the corresponding state", () {
      when(repository.getUserInfo()).thenAnswer((_) => Future.value(const UserInfo(email: "some.user@inno.uni")));

      homeBloc.add(CheckAuthEvent());
      expect(homeBloc.stream, emits(AuthorizedState(const UserInfo(email: "some.user@inno.uni"))));
    });
  });

  group("check in bloc", () {
    late CheckInBloc checkInBloc;

    setUp(() {
      checkInBloc = CheckInBloc(repository);
    });

    test("scanning an invalid code produces an error", () {
      when(repository.submitChallenge(any, any)).thenThrow(MalformedQRCode());
      expect(checkInBloc.state, ScanningState());

      checkInBloc.add(ParseAndSubmitCodeEvent("some random data from QR"));
      expect(checkInBloc.stream, emits(ScanningState(exception: MalformedQRCode())));
    });

    test("scanning an outdated code produces an error", () {
      when(repository.submitChallenge(any, any)).thenThrow(CodeIsUnacceptableException());
      expect(checkInBloc.state, ScanningState());

      checkInBloc.add(ParseAndSubmitCodeEvent("https://baam.duckdns.org/s#123-Ltg6QiET0"));
      expect(checkInBloc.stream, emits(ScanningState(exception: CodeIsUnacceptableException())));
    });

    test("scanning a valid code changes the state", () {
      when(repository.submitChallenge(any, any)).thenAnswer((_) => Future(() {}));
      when(repository.getMarkedSessionInfo(any)).thenAnswer(
        (_) => Future.value(
          const MarkedSessionInfo("id", "title", []),
        ),
      );
      expect(checkInBloc.state, ScanningState());

      checkInBloc.add(ParseAndSubmitCodeEvent("https://baam.duckdns.org/s#123-Ltg6QiET0"));
      expect(checkInBloc.stream, emits(CheckedInState(const MarkedSessionInfo("id", "title", []))));
    });
  });

  group("new session bloc", () {
    late NewSessionBloc newSessionBloc;

    const session = RunningSessionInfo("id", "title", <UserInfo>[], "seed", 1500, 0);
    sessionCode(i) => "https://baam.duckdns.org/s#id-${session.getAttendanceCode(i)}";

    setUp(() {
      newSessionBloc = NewSessionBloc(repository);
    });

    test("on session start state should increment except first time", () {
      when(repository.getOrStartSession()).thenAnswer(
        (_) => Future.value(session),
      );
      when(repository.getSessionInfo(any)).thenAnswer(
        (_) => Future.value(const SessionInfo("id", "title", [])),
      );

      newSessionBloc.add(GetOrStartSessionEvent());
      expect(
        newSessionBloc.stream,
        emitsInOrder([
          const LoadingState(),
          RunningSessionState(const SessionInfo("id", "title", []), sessionCode(0)),
          RunningSessionState(const SessionInfo("id", "title", []), sessionCode(1)),
          RunningSessionState(const SessionInfo("id", "title", []), sessionCode(2)),
        ]),
      );
    });

    test("users fetched from API should be added", () {
      when(repository.getOrStartSession()).thenAnswer(
        (_) => Future.value(session),
      );
      when(repository.getSessionInfo(any)).thenAnswer(
        (_) => Future.value(const SessionInfo("id", "title", [UserInfo(email: "email")])),
      );

      newSessionBloc.add(GetOrStartSessionEvent());
      expect(
        newSessionBloc.stream,
        emitsInOrder([
          const LoadingState(),
          RunningSessionState(const SessionInfo("id", "title", []), sessionCode(0)),
          RunningSessionState(const SessionInfo("id", "title", [UserInfo(email: "email")]), sessionCode(0)),
          RunningSessionState(const SessionInfo("id", "title", [UserInfo(email: "email")]), sessionCode(1)),
        ]),
      );
    });


    test("users added manually should call correct API point", () async {
      const session = RunningSessionInfo("id", "title", <UserInfo>[], "seed", 10, 0);
      when(repository.getOrStartSession()).thenAnswer(
        (_) => Future.value(session),
      );
      when(repository.getSessionInfo(any)).thenAnswer(
        (_) => Future.value(const SessionInfo("id", "title", [])),
      );

      newSessionBloc.add(GetOrStartSessionEvent());
      newSessionBloc.add(AddUserEvent("email"));

      await untilCalled(repository.addUserToSession(session.id, userEmail: "email"));
      verify(repository.addUserToSession(session.id, userEmail: "email")).called(1);
    });

    test("session rename should call correct API point", () async {
      when(repository.getOrStartSession()).thenAnswer(
        (_) => Future.value(session),
      );
      when(repository.getSessionInfo(any)).thenAnswer(
        (_) => Future.value(const SessionInfo("id", "title", [])),
      );

      newSessionBloc.add(GetOrStartSessionEvent());
      newSessionBloc.add(RenameSessionEvent("another title"));

      await untilCalled(repository.changeSessionInfo(session.id, title: "another title"));
      verify(repository.changeSessionInfo(session.id, title: "another title")).called(1);
    });


    test("session reset should return to lading state", () {
      when(repository.getOrStartSession()).thenAnswer(
        (_) => Future.value(session),
      );
      when(repository.getSessionInfo(any)).thenAnswer(
        (_) => Future.value(const SessionInfo("id", "title", [])),
      );
      when(repository.deleteSession(any)).thenAnswer(
        (_) => Future(() {}),
      );

      newSessionBloc.add(GetOrStartSessionEvent());
      Future.delayed(const Duration(milliseconds: 50), () => newSessionBloc.add(ResetSessionEvent()));

      expect(
        newSessionBloc.stream,
        emitsInOrder([
          const LoadingState(),
          RunningSessionState(const SessionInfo("id", "title", []), sessionCode(0)),
          const LoadingState(),
          RunningSessionState(const SessionInfo("id", "title", []), sessionCode(0)),
        ]),
      );
    });

    test("session stop should switch to the stopped state", () {
      when(repository.getOrStartSession()).thenAnswer(
        (_) => Future.value(session),
      );
      when(repository.getSessionInfo(any)).thenAnswer(
        (_) => Future.value(const SessionInfo("id", "title", [])),
      );
      when(repository.stopSession(any)).thenAnswer(
        (_) => Future(() {}),
      );

      newSessionBloc.add(GetOrStartSessionEvent());
      Future.delayed(const Duration(milliseconds: 50), () => newSessionBloc.add(StopSessionEvent()));

      expect(
        newSessionBloc.stream,
        emitsInOrder([
          const LoadingState(),
          RunningSessionState(const SessionInfo("id", "title", []), sessionCode(0)),
          const StopSessionLoadingState(SessionInfo("id", "title", [])),
          const StoppedSessionState(SessionInfo("id", "title", []))
        ]),
      );
    });
  });

  group("session list bloc", () {
    late SessionListBloc sessionListBloc;

    setUp(() {
      sessionListBloc = SessionListBloc(repository);
      when(repository.getCollectedSessions()).thenAnswer(
        (_) => Future.value(
          [
            PartialSessionInfo("id", "title", DateTime(2022, 11, 30, 10, 0), 5),
            PartialSessionInfo("id", "title", DateTime(2022, 12, 1, 12, 0), 11),
          ],
        ),
      );
    });

    test("session list load should switch the state", () async {
      sessionListBloc.add(SessionListLoadEvent());

      await expectLater(sessionListBloc.stream, emits(SessionListLoadedState()));
      expect(sessionListBloc.sessions, [
        PartialSessionInfo("id", "title", DateTime(2022, 11, 30, 10, 0), 5),
        PartialSessionInfo("id", "title", DateTime(2022, 12, 1, 12, 0), 11),
      ]);
    });

    test("session list reload should switch to the initial state", () {
      sessionListBloc.add(SessionListLoadEvent());
      Future.delayed(const Duration(milliseconds: 5), () => sessionListBloc.add(SessionListReloadEvent()));

      expect(
        sessionListBloc.stream,
        emitsThrough(SessionListInitial()),
      );
    });

    test("session deletion should switch the state", () async {
      sessionListBloc.add(SessionListLoadEvent());
      Future.delayed(
        const Duration(milliseconds: 5),
        () => sessionListBloc.add(
          SessionListDeleteEvent(
            PartialSessionInfo("id", "title", DateTime(2022, 11, 30, 10, 0), 5),
          ),
        ),
      );

      await expectLater(
        sessionListBloc.stream,
        emitsThrough(SessionDeletedState()),
      );

      expect(sessionListBloc.sessions, [
        PartialSessionInfo("id", "title", DateTime(2022, 12, 1, 12, 0), 11),
      ]);
    });
  });
}
