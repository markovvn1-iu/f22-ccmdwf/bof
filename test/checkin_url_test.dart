import "package:baam/network/models/baam_models.dart";
import "package:flutter_test/flutter_test.dart";

RunningSessionInfo getSession({required String seed}) {
  return RunningSessionInfo("", "", const <UserInfo>[], seed, 0, 0);
}

void main() {
  test("Check in URL correctness", () {
    expect(
      getSession(seed: "l4qxr47l2xw8").getAttendanceCode(0),
      "Ltg6QiET0",
    );
    expect(
      getSession(seed: "1rke2o3g1mltr").getAttendanceCode(98),
      "8A_a73ZD2q",
    );
    expect(
      getSession(seed: "l4qxr47l2xw8").getAttendanceCode(11),
      "r+zkcTSzb",
    );
    expect(
      getSession(seed: "l4qxr47l2xw8").getAttendanceCode(35),
      "Y_eB+Zw2z",
    );
  });
}
