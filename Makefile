.PHONY: lint
lint:
	flutter format lib --line-length=120 --set-exit-if-changed
	flutter analyze


.PHONY: format
format:
	flutter format lib --line-length=120


.PHONY: test
test:
	flutter test


.PHONY: build
build:
	flutter build apk
