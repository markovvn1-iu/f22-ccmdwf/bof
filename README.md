<div align="center">
  <img src=".gitlab/logo.svg" height="124px"/>

# [Baam](http://baam.duckdns.org/) on Flutter (BoF)

  Android application for the Best Automated Attendance Monitoring
</div>

## 📝 About The Project

This repository contains an Android app written in Flutter for the the [Best Automated Attendance Monitoring](https://gitlab.com/inno_baam/baam). The application is an adaptation of the BAAM [website](http://baam.duckdns.org/) for android, which makes it even easier to mark attendance.

A list of features and a the app demo can be found in our wiki: [list of features](https://gitlab.com/markovvn1-iu/f22-ccmdwf/bof/-/wikis/Application-features), [demo](https://gitlab.com/markovvn1-iu/f22-ccmdwf/bof/-/wikis/Applicaton-demo)

## 🚀 Try App

Try our application using latest build artifact: :link: [Download latest APK for Android](https://gitlab.com/markovvn1-iu/f22-ccmdwf/bof/-/jobs/artifacts/main/raw/baam.apk?job=build)

### ⚙️ Developing

1. [Install Dart and Flutter](https://flutter.dev/docs/get-started/install) and android SDK
2. Clone the project `git clone https://gitlab.com/markovvn1-iu/f22-ccmdwf/bof.git && cd bof`
3. Build release version using `make build` command

## 💻 Contributors

🎓 *All participants in this project are undergraduate students in the [Department of Computer Science](https://apply.innopolis.university/en/bachelor/) **@** [Innopolis University](https://innopolis.university/)*

👦 **Ahmed Nouralla** <br>
&emsp;&ensp; GitHub: [@sh3b0](https://github.com/sh3b0)

👦 **Sherif Nafee** <br>
&emsp;&ensp; Email: <a>sherif.m.nafee@gmail.com</a> <br />
&emsp;&ensp; GitHub: [@Sh1co](https://github.com/Sh1co)

👦 **Vladimir Markov** <br>
&emsp;&ensp; Email: <a>Markovvn1@gmail.com</a> <br />
&emsp;&ensp; GitLab: [@markovvn1](https://gitlab.com/markovvn1)

👦 **Vladislav Safonov** <br>
&emsp;&ensp; GitHub: [@ntdesmond](https://github.com/ntdesmond)
